<?php

use Nette\Application;
use Nette\FileNotFoundException;
use Nette\Http;
use Nette\InvalidArgumentException;
use Nette\InvalidStateException;
use Nette\Utils;

/**
 * Class PdfResponse
 * Wrapper of mPDF for Nette Framework.
 *
 * @author Jaroslav Hranička
 * @author Jan Kuchař
 * @author Tomáš Votruba
 * @author Miroslav Paulík
 *
 * @copyright (c) 2014 Jaroslav Hranička
 * @copyright (c) 2010 Jan Kuchař (original code)
 * @license LGPL
 */
class PdfResponse implements Application\Response
{

	use Nette\SmartObject;

	const INLINE = 'I';
	const DOWNLOAD = 'D';

	/**
	 * Portrait page orientation
	 */
	const ORIENTATION_PORTRAIT = 'P';

	/**
	 * Landscape page orientation
	 */
	const ORIENTATION_LANDSCAPE = 'L';

	const COLOR_SPACE_GRAYSCALE = 1;
	const COLOR_SPACE_RGB = 2;
	const COLOR_SPACE_CMYK = 3;

	/**
	 * Specifies page orientation.
	 * You can use constants:
	 * <ul>
	 *   <li>PdfResponse::ORIENTATION_PORTRAIT (default)
	 *   <li>PdfResponse::ORIENTATION_LANDSCAPE
	 * </ul>
	 *
	 * @var string
	 */
	public $pageOrientation = self::ORIENTATION_PORTRAIT;

	/**
	 * Specifies format of the document<br>
	 * <br>
	 * Allowed values: (Values are case-<b>in</b>sensitive)
	 * <ul>
	 *   <li>A0 - A10
	 *   <li>B0 - B10
	 *   <li>C0 - C10
	 *   <li>4A0
	 *   <li>2A0
	 *   <li>RA0 - RA4
	 *   <li>SRA0 - SRA4
	 *   <li>Letter
	 *   <li>Legal
	 *   <li>Executive
	 *   <li>Folio
	 *   <li>Demy
	 *   <li>Royal
	 *   <li>A<i> (Type A paperback 111x178mm)</i>
	 *   <li>B<i> (Type B paperback 128x198mm)</i>
	 * </ul>
	 *
	 * @var string
	 */
	public $pageFormat = 'A4';

	/**
	 * @var int
	 * 0 - no check
	 * 1 - allow GRAYSCALE only [convert CMYK/RGB->gray]
	// 2 - allow RGB / SPOT COLOR / Grayscale [convert CMYK->RGB]
	// 3 - allow CMYK / SPOT COLOR / Grayscale [convert RGB->CMYK]
	 */
	public $colorSpace = 0;

	/**
	 * Margins in this order:
	 * <ol>
	 *   <li>top
	 *   <li>right
	 *   <li>bottom
	 *   <li>left
	 *   <li>header
	 *   <li>footer
	 * </ol>
	 * Please use values <b>higer than 0</b>. In some PDF browser zero values may
	 * cause problems!
	 *
	 * @var string
	 */
	public $pageMargins = '16,15,16,15,9,9';

	/**
	 * Author of the document
	 *
	 * @var string
	 */
	public $documentAuthor = 'PDF Response';

	/**
	 * Title of the document
	 *
	 * @var string
	 */
	public $documentTitle = 'Unnamed document';

	/**
	 * This parameter specifies the magnification (zoom) of the display when the document is opened.<br>
	 * Values (case-<b>sensitive</b>)
	 * <ul>
	 *   <li><b>fullpage</b>: Fit a whole page in the screen
	 *   <li><b>fullwidth</b>: Fit the width of the page in the screen
	 *   <li><b>real</b>: Display at real size
	 *   <li><b>default</b>: User's default setting in Adobe Reader
	 *   <li><i>integer</i>: Display at a percentage zoom (e.g. 90 will display at 90% zoom)
	 * </ul>
	 *
	 * @var string|int
	 */
	public $displayZoom = 'default';

	/**
	 * Specify the page layout to be used when the document is opened.<br>
	 * Values (case-<b>sensitive</b>)
	 * <ul>
	 *   <li><b>single</b>: Display one page at a time
	 *   <li><b>continuous</b>: Display the pages in one column
	 *   <li><b>two</b>: Display the pages in two columns
	 *   <li><b>default</b>: User's default setting in Adobe Reader
	 * </ul>
	 *
	 * @var string
	 */
	public $displayLayout = 'continuous';

	/**
	 * Before document output starts
	 *
	 * @var callable
	 */
	public $onBeforeComplete = [];

	/**
	 * Multi-language document?
	 *
	 * @var bool
	 */
	public $multiLanguage = FALSE;

	/**
	 * Additional stylesheet as a <b>string</b>
	 *
	 * @var string
	 */
	public $styles = '';

	/**
	 * <b>Ignore</b> styles in HTML document
	 * When using this feature, you MUST also install SimpleHTMLDom to your application!
	 *
	 * @var bool
	 */
	public $ignoreStylesInHTMLDocument = FALSE;

	/** @var string */
	private $tempDir;

	/** @var string path to (PDF) file */
	private $backgroundTemplate;

	/** @var string save mode */
	private $saveMode = self::DOWNLOAD;

	/** @var array Source data */
	private $sources = [];

	/** @var Mpdf\Mpdf */
	private $mpdf;

	/** @var array */
	private $fonts = [];

	/**
	 * @param mixed $source Template content.
	 */
	public function __construct($source = NULL)
	{
		if ($source) {
			$this->addSource($source);
		}
	}

	public function setTempDir($tempDir)
	{
		$this->tempDir = $tempDir;
		return $this;
	}

	public function addFont($name, $fontPath)
	{
		$this->fonts[$name] = $fontPath;
	}

	/**
	 * @param mixed $source Template content.
	 * @return $this
	 */
	public function addSource($source)
	{
		$this->sources[] = $source;
		return $this;
	}

	/**
	 * @param string $pathToBackgroundTemplate
	 * @throws FileNotFoundException
	 */
	public function setBackgroundTemplate($pathToBackgroundTemplate)
	{
		if (!file_exists($pathToBackgroundTemplate)) {
			throw new FileNotFoundException("File '$pathToBackgroundTemplate' not found.");
		}
		$this->backgroundTemplate = $pathToBackgroundTemplate;
	}

	/**
	 * Sends response to output
	 *
	 * @param Http\IRequest $httpRequest
	 * @param Http\IResponse $httpResponse
	 * @return void
	 */
	public function send(Http\IRequest $httpRequest, Http\IResponse $httpResponse): void
	{
		$this->render();
	}

	/**
	 * Sends response to output
	 *
	 * @return void
	 */
	public function render()
	{
		$mpdf = $this->build();
		$mpdf->Output(Utils\Strings::webalize($this->documentTitle) . '.pdf', $this->saveMode);
	}

	/**
	 * Save file to target location
	 *
	 * @param string $location
	 * @param string $name
	 * @return string
	 */
	public function save($location, $name = NULL)
	{
		$pdf = $this->build();
		$file = $pdf->Output($name, 'S');
		$name = Utils\Strings::webalize($name ?: $this->documentTitle) . '.pdf';

		$filePath = rtrim($location, '/\\') . '/' . $name;
		Utils\FileSystem::createDir($location);
		file_put_contents($filePath, $file);

		return $filePath;
	}

	/**
	 * Returns Mpdf object
	 *
	 * @return Mpdf\Mpdf
	 */
	public function getMPDF()
	{
		if (!$this->mpdf instanceof Mpdf\Mpdf) {
			$this->mpdf = $this->createMPDF();
		}

		return $this->mpdf;
	}

	/**
	 * To force download, use PdfResponse::DOWNLOAD
	 * To show pdf in browser, use PdfResponse::INLINE
	 *
	 * @param string $saveMode
	 * @throws InvalidArgumentException
	 */
	public function setSaveMode($saveMode)
	{
		if (!in_array($saveMode, [self::DOWNLOAD, self::INLINE])) {
			throw new InvalidArgumentException('Invalid mode');
		}
		$this->saveMode = $saveMode;
	}

	/**
	 * Getts margins as array
	 *
	 * @throws InvalidStateException
	 * @throws InvalidArgumentException
	 * @return array
	 */
	private function getMargins()
	{
		$margins = explode(',', $this->pageMargins);
		if (count($margins) !== 6) {
			throw new InvalidStateException('You must specify all margins! For example: 16,15,16,15,9,9');
		}

		$dictionary = [
			0 => 'top',
			1 => 'right',
			2 => 'bottom',
			3 => 'left',
			4 => 'header',
			5 => 'footer',
		];

		$marginsOut = [];
		foreach ($margins as $key => $val) {
			$val = (int)$val;
			if ($val < 0) {
				throw new InvalidArgumentException('Margin must not be negative number!');
			}
			$marginsOut[$dictionary[$key]] = $val;
		}

		return $marginsOut;
	}

	/**
	 * Creates and returns mPDF object
	 *
	 * @internal param \PdfResponse $response
	 * @return Mpdf\Mpdf
	 */
	private function createMPDF()
	{
		$config = [
			'mode' => 'utf-8',
			'format' => $this->pageFormat,
			'default_font_size' => 0,
			'default_font' => '',
			'orientation' => $this->pageOrientation,
		];

		$margins = $this->getMargins();
		$config['margin_left'] = $margins['left'];
		$config['margin_right'] = $margins['right'];
		$config['margin_top'] = $margins['top'];
		$config['margin_bottom'] = $margins['bottom'];
		$config['margin_header'] = $margins['header'];
		$config['margin_footer'] = $margins['footer'];

		$temp = $this->getTempDir();
		if ($temp) {
			$config['tempDir'] = $temp;
			$config['fontTempDir'] = $temp;
		}

		if (count($this->fonts) > 0) {
			$config = $this->fontsToConfig($config);
		}

		$config['restrictColorSpace'] = $this->colorSpace;

		return new Mpdf\Mpdf($config);
	}

	private function getTempDir()
	{
		if ($this->tempDir) {
			$temp = $this->tempDir;
		} elseif (defined('TEMP_DIR')) {
			// Back-compatibility with previous versions.
			// Should be removed later.
			$temp = TEMP_DIR . '/mpdf';
		} else {
			$temp = NULL;
		}

		if ($temp) {
			\Nette\Utils\FileSystem::createDir($temp);
		}

		return $temp;
	}

	/**
	 * Build final pdf
	 * @throws \Exception
	 * @return Mpdf\Mpdf
	 */
	private function build()
	{
		if (empty($this->documentTitle)) {
			throw new \Exception("Variable 'documentTitle' cannot be empty.");
		}

		if ($this->backgroundTemplate) {
			// if background exists, then add it as a background
			$mpdf = $this->getMPDF();
			$mpdf->SetImportUse();
			$mpdf->AddPage();
			$pagecount = $mpdf->SetSourceFile($this->backgroundTemplate);
			$tplId = $mpdf->ImportPage($pagecount);
			$mpdf->UseTemplate($tplId);
		}

		$mpdf = $this->getMPDF();
		$mpdf->biDirectional = $this->multiLanguage;
		$mpdf->SetAuthor($this->documentAuthor);
		$mpdf->SetTitle($this->documentTitle);
		$mpdf->SetDisplayMode($this->displayZoom, $this->displayLayout);
		$mpdf->showImageErrors = TRUE;

		$this->writeHtml($mpdf);

		// Add styles
		if (!empty($this->styles)) {
			$mpdf->WriteHTML($this->styles, 1);
		}

		$this->onBeforeComplete($mpdf);

		return $mpdf;
	}

	/**
	 * @param mixed $source
	 * @return string
	 */
	private function buildHtml($source)
	{
		if ($source instanceof Application\UI\Template) {
			$source->pdfResponse = $this;
			$source->mPDF = $this->getMPDF();

			ob_start();
			$source->render();
			$html = ob_get_clean();
		} else {
			$html = (string)$source;
		}

		// Fix: $html can't be empty (mPDF generates Fatal error)
		if (!$html) {
			$html = '<html><body></body></html>';
		}

		return $html;
	}

	/**
	 * @param Mpdf\Mpdf $mpdf
	 */
	private function writeHtml(Mpdf\Mpdf $mpdf)
	{
		$counter = 0;
		foreach ($this->sources as $source) {
			$html = $this->buildHtml($source);

			// @see: http://mpdf1.com/manual/index.php?tid=121&searchstring=writeHTML
			if ($this->ignoreStylesInHTMLDocument) {
				$html = $this->removeDocumentStyles($html);
				$mode = 2; // If <body> tags are found, all html outside these tags are discarded, and the rest is parsed as content for the document. If no <body> tags are found, all html is parsed as content. Prior to mPDF 4.2 the default CSS was not parsed when using mode #2
			} else {
				$mode = 0; // Parse all: HTML + CSS
			}

			// Sanitize
			$html = $this->removeInvalidStyles($html);

			// Add content
			if ($counter++) {
				$mpdf->AddPage();
			}

			$mpdf->WriteHTML($html, $mode);
		}
	}

	/**
	 * @param string $html
	 * @return string
	 */
	private function removeDocumentStyles($html)
	{
		$html = preg_replace('~<!--mpdf~i', '', $html);
		$html = preg_replace('~mpdf-->~i', '', $html);
		$html = preg_replace('~<\!\-\-.*?\-\->~s', '', $html);
		$html = preg_replace('~<style.*?><\/style>~s', '', $html);

		return $html;
	}

	private function removeInvalidStyles($html)
	{
		$html = preg_replace('~(\s*[a-zA-Z0-9_-]+:\s*inherit;?\s*)~i', '', $html);

		return $html;
	}

	private function fontsToConfig($config)
	{
		$configVariables = new \Mpdf\Config\ConfigVariables();
		$defaultConfig = $configVariables->getDefaults();

		$fontVariables = new \Mpdf\Config\FontVariables();
		$defaultFontConfig = $fontVariables->getDefaults();

		$fontDirs = $defaultConfig['fontDir'];
		$fontData = $defaultFontConfig['fontdata'];

		foreach ($this->fonts as $name => $fontPath) {
			if (!file_exists($fontPath)) {
				throw new FileNotFoundException("Font file '$fontPath' not found.");
			}
			$fontDir = pathinfo($fontPath, PATHINFO_DIRNAME);
			if (!in_array($fontDir, $fontDirs)) {
				$fontDirs[] = $fontDir;
			}
			$fontFileName = pathinfo($fontPath, PATHINFO_BASENAME);
			$fontData[$name] = [
				'R' => $fontFileName,
			];
		}

		$config['fontDir'] = $fontDirs;
		$config['fontdata'] = $fontData;

		return $config;
	}

}
