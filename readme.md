# PDF Response - mPDF wrapper

* sends template as PDF output
* saves template as PDF file
* requiers mPDF & Nette

## Installation

Recommended installation is via Composer.

    {
        "require":{
            "hranicka/pdf-response": "~4.0"
        }
    }

## How to prepare PDF from template

    use PdfResponse;
    
    class MyPresenter extends Nette\Application\UI\Presenter
    {
    
        public function actionPdf()
        {
            $template = $this->createTemplate()->setFile(APP_DIR . "/templates/myPdf.latte");
            $template->someValue = 123;
            // Tip: In template to make a new page use <pagebreak>
    
            $pdf = new PdfResponse($template);
    
            // optional
            $pdf->documentTitle = date("Y-m-d") . " My super title"; // creates filename 2012-06-30-my-super-title.pdf
            $pdf->pageFormat = "A4-L"; // wide format
            $pdf->getMPDF()->setFooter("|© www.mysite.com|"); // footer
        }
        
    }

## Save file in server

    use PdfResponse;

    public function actionPdf()
    {
        $template = $this->createTemplate()->setFile(APP_DIR . "/templates/myPdf.latte");
    
        $pdf = new PdfResponse($template);
    
        $pdf->save(WWW_DIR . "/generated/"); // as a filename $this->documentTitle will be used
        $pdf->save(WWW_DIR . "/generated/", "another file 123); // OR use a custom name
    }


## Attach file to email

    public function actionPdf()
    {
        $template = $this->createTemplate()->setFile(APP_DIR . "/templates/myPdf.latte");
        
        $pdf = new PdfResponse($template);
        
        $savedFile = $pdf->save(WWW_DIR . "/contracts/");
        $mail = new Nette\Mail\Message;
        $mail->addTo("john@doe.com");
        $mail->addAttachment($savedFile);
        $mail->send();
    }
    

## Force file to download

    public function actionPdf()
    {
        $template = $this->createTemplate()->setFile(APP_DIR . "/templates/myPdf.latte");
        
        $pdf = new PdfResponse($template);
        
        $pdf->setSaveMode(PdfResponse::DOWNLOAD); //default behavior
        
        $this->sendResponse($pdf);
    }
    

## Force file to display in a browser

    public function actionPdf()
    {
        $template = $this->createTemplate()->setFile(APP_DIR . "/templates/myPdf.latte");
        
        $pdf = new PdfResponse($template);
        
        $pdf->setSaveMode(PdfResponse::INLINE);
        
        $this->sendResponse($pdf);
    }
    

## Set a pdf background easily

    public function actionPdf()
    {
        $template = $this->createTemplate()->setFile(APP_DIR . "/templates/myPdf.latte");
        
        $pdf = new PdfResponse($template);
        
        $pdf->setBackgroundTemplate(APP_DIR . "/templates/PDF_template.pdf");
        
        $this->sendResponse($pdf);
    }


## Simple setting custom fonts

	public function actionPdf()
    {
        $template = $this->createTemplate()->setFile(APP_DIR . "/templates/myPdf.latte");
            
        $pdf = new PdfResponse($template);
            
        $pdf->setFont('gotham-light', APP_DIR . "/www/fonts/gotham/Gotham-Light.ttf');
            
        $this->sendResponse($pdf);
     }


## More info

* http://forum.nette.org/cs/9037-zkusenosti-s-pouzitim-pdfresponse-v-nette-2-0b-a-php-5-3
* http://forum.nette.org/cs/3726-addon-pdfresponse-pdfresponse
* http://addons.nette.org/cs/pdfresponse
